FROM node
RUN mkdir -p /src/app/archivos
WORKDIR /src/app/archivos
COPY . .
RUN npm install -g @angular/cli
RUN npm install 
EXPOSE 4200
CMD ng serve --host 0.0.0.0 --disable-host-check
